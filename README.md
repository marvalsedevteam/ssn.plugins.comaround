# SSN.Plugins.ComAround

## Information

	With this plugin, MSM Servicedesk user will be able to search and read article from ComAround knowledge base.
	It is also possible to solve MSM request with ComAround article.

## ComAround
	
	ComAround is a knowledge base system https://www.comaround.com

## Installation

	Please see your MSM documentation for information on how to install plugins.

	Copy jquery.bpopup.js under "MSM\Service Desk\Scripts\"

	Add following reference code to file "MSM\Service Desk\RFP\Assets\Scripts\MarvalSoftware\UI\Controls\KnowledgeMatcher.js"
		/// <reference path="~/Scripts/jquery.bpopup.js"/>
	
	Go to Database maintenence to clear cache

## Dependence of MSM code

	- _knowledgeMatcher._setIndicatorVisibility. Overwritten to make it suitable for integration
	- _knowledgeMatcher._initiateSearches. Overwritten to make it suitable for integration

## Configuration
	
	- ComaroundAPIAddress:		ComAround API address https://api2.comaround.com/rest/v1
	- ComaroundAPIVersion:		v1, v2 or v3. It needs to be same as the number in ComaroundAPIAdress
    - ComaroundAPIUsername:		ComAround API username
    - ComaroundAPIPassword:		ComAround API password
    - ComaroundAPISubKey:		ComAround API subscrition key which can be obtained from https://connect.comaround.com/
    - ComaroundAddress:			ComAround web address https://zero.comaround.com
    - ComaroundClientKey:		ComAround client key for automatic login
	- ComaroundResultAmount		How many article will be displayed in the search result
    - ResultHeaderText:			Header text in ComAround search result element
    - ExtendSearchLinkText:		Text for extern search link
    - OpenInNewPageButtonText:	Text for button to open article in new page on popup
    - SolveRequestButtonText:	Text for button to solve request in new page on popup
    - SolutionPrefix:			Prefix of solution when solving request from ComAround article
	- ClassificationWhenSolving: Primary classifications will be set automatically when solving request from Comaround article. Configuration needs to be in JSON format, for example 
										{ '18':{ 'Identifier': 105, 'Description': 'Reason 2' }, '12': { 'Identifier': 93, 'Description': 'Known error' }}
	- ProxyAddress:				Address to proxy with port number
	- KeylessUser:				List of users who have own login account to comaround which don't need to use comaroundClientKey. It is a list of Ids seperated by ";", for example
										1;2;3;4

## Layout
	
	Layout code to customize search result and popup can be found in custom.css.
	CSS code needs to be appended to existing custom.css in system.

## Supported browser
	
	Chrome
	Firefox
	Edge
	IE 11

## Functions

### 1.0
	
	- Move code from old MSMExtension to plugin
	- Move all configurations from code to global settings

## Compatible Versions

| Plugin   | MSM         |
|----------|-------------|
| 1.0	   | 14.10       |