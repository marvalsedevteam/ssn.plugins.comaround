<%@ WebHandler Language="C#" Class="ComaroundHandler" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Web.Caching;
using System.ServiceModel;
using System.Net.Http.Headers;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using MarvalSoftware.UI.WebUI.ServiceDesk.RFP.Plugins;

/// <summary>
/// Represents the checklist HTTP handler.
/// </summary>
public class ComaroundHandler : PluginHandler
{
    #region Properties

    /// <summary>
    /// Gets or sets whether or not this handler is reusable.
    /// </summary>
    public override bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private string ComaroundAPIAddress
    {
        get
        {
            return GlobalSettings["ComaroundAPIAddress"];
        }
    }

    private string ComaroundAPIVersion
    {
        get
        {
            return GlobalSettings["ComaroundAPIVersion"];
        }
    }

    private string ComaroundAPIUsername
    {
        get
        {
            return GlobalSettings["ComaroundAPIUsername"];
        }
    }

    private string ComaroundAPIPassword
    {
        get
        {
            return GlobalSettings["ComaroundAPIPassword"];
        }
    }

    private string ComaroundAPISubKey
    {
        get
        {
            return GlobalSettings["ComaroundAPISubKey"];
        }
    }

    private string ComaroundResultAmount
    {
        get
        {
            return GlobalSettings["ComaroundResultAmount"];
        }
    }

    private string ProxyAddress
    {
        get
        {
            return GlobalSettings["ProxyAddress"];
        }
    }

    #endregion

    public class Token
    {
        public string Access_Token {get; set;}
        public string Token_Type {get; set;}
        public int Expires_In {get; set;}
    }

    #region Methods

    /// <summary>
    /// Processes the HTTP request.
    /// </summary>
    /// <param name="context">The HTTP context.</param>
    /// <remarks>Gets a checklist from a given service identifier.</remarks>
    public override void HandleRequest(HttpContext context)
    {

        if (context.Request.QueryString["f"] == "Search")
        {
            string keyword = context.Request.QueryString["keyword"];
            string type = context.Request.QueryString["type"];

            // add support to tls 1.1 and 1.2
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;

            if (keyword.Length > 0)
            {
                // Ceate http client
                var client = new HttpClient();
                
                // If need to use proxy
                if(!String.IsNullOrEmpty(this.ProxyAddress))
                {
                    // Create handler
                    HttpClientHandler clientHandler = new HttpClientHandler
                    {
                        Proxy =  new System.Net.WebProxy(this.ProxyAddress, false)
                    };

                    client = new HttpClient(handler:clientHandler, disposeHandler: true);
                }

                var queryString = "SearchPhrase=" + keyword +
                                  "&ItemsPerPage=" + this.ComaroundResultAmount +
                                  "&SortBy=" + "MostRelevant" +
                                  "&Tags=" + "Video" +
                                  "&Tags=" + "Text";

                var uri = this.ComaroundAPIAddress + "/search/?" + queryString;

                // set the response content type
                context.Response.ContentType = "application/json";
                if(HttpRuntime.Cache.Get("StandardComaroundToken") == null)
                {
                    client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", this.ComaroundAPISubKey);

                    if (this.ComaroundAPIVersion.ToLower() == "v1")
                    {
                        var tokenUri = this.ComaroundAPIAddress + "/token/?" + "username=" + this.ComaroundAPIUsername + "&password=" + this.ComaroundAPIPassword;
                        var tokenResponse = client.PostAsync(tokenUri, new StringContent(String.Empty)).Result;

                        if (tokenResponse.IsSuccessStatusCode)
                        {
                            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                            Token tokenJSON = oSerializer.Deserialize<Token>(tokenResponse.Content.ReadAsStringAsync().Result);

                            DateTime expires = DateTime.UtcNow.AddSeconds(tokenJSON.Expires_In - 600);
                            HttpRuntime.Cache.Insert("StandardComaroundToken", "bearer " + tokenJSON.Access_Token, null, expires, System.Web.Caching.Cache.NoSlidingExpiration);
                        }
                    }
                    if (this.ComaroundAPIVersion.ToLower() == "v2")
                    {
                        var tokenUri = this.ComaroundAPIAddress + "/token/";

                        var tokenResponse = client.PostAsync(tokenUri, new StringContent("{\r\n  \"username\": \""+ this.ComaroundAPIUsername +"\",\r\n  \"password\": \""+ this.ComaroundAPIPassword +"\"\r\n}")).Result;

                        if (tokenResponse.IsSuccessStatusCode)
                        {
                            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                            Token tokenJSON = oSerializer.Deserialize<Token>(tokenResponse.Content.ReadAsStringAsync().Result);

                            DateTime expires = DateTime.UtcNow.AddSeconds(tokenJSON.Expires_In - 600);
                            HttpRuntime.Cache.Insert("StandardComaroundToken", "bearer " + tokenJSON.Access_Token, null, expires, System.Web.Caching.Cache.NoSlidingExpiration);
                        }
                    }
                }

                string standardAuthToken = (string) HttpRuntime.Cache.Get("StandardComaroundToken");

                if(type == "Standard")
                {
                    // Request headers
                    client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", this.ComaroundAPISubKey);
                    client.DefaultRequestHeaders.Add("Authorization", standardAuthToken);
                }
                else if(type == "Professional")
                {
                    // Request headers
                    client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", this.ComaroundAPISubKey);
                    client.DefaultRequestHeaders.Add("Authorization", standardAuthToken);
                }

                var response = client.GetAsync(uri).Result;

                if (response.IsSuccessStatusCode)
                {
                    string content = response.Content.ReadAsStringAsync().Result;
                    context.Response.Write(content);
                }
                else
                {
                    context.Response.Write(String.Empty);
                }

                client.Dispose();
            }
        }
    }

    #endregion
}